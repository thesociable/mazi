# :coding: utf-8
# :copyright: Copyright (C) 2011 Martin Pengelly-Phillips
# :license: Proprietary. See LICENSE.txt for details.

import os
import re
import logging

FILM, TV, UNKNOWN = ('film', 'tv', 'unknown')

TIMINGS = {
    FILM: {'min':5400, 'max':10800}, # 90-180 mins
    TV: {'min':1080, 'max':4800}, # 18-80 mins
}

TITLE_REG = re.compile('([a-z])([A-Z])')


class Media(object):
    '''Media'''

    def __init__(self, path=None, titles=None, **kw):
        '''Initialise.
        
        *path* is the location of the media.

        '''
        self.log = logging.getLogger('mazi.media.Media')
        self.path = path
        self.titles = titles
        self.name = 'Unknown'
        self.type = UNKNOWN
        self._distinguish()

    def __str__(self):
        return '{0.name} ({0.type})'.format(self)

    @property
    def safe_name(self):
        '''Return filename safe version of name'''
        return self.name.lower()

    def _distinguish(self):
        '''Process data further to set name, type and limit titles.
        
        Look at titles and try to determine type of media based on duration
        of titles.

        '''
        # Name
        name = os.path.basename(self.path)
        name = TITLE_REG.sub('\g<1> \g<2>', name)
        name = name.replace('_', ' ')
        name = name.split('(')[0].strip()
        name = name.split('16X9')[0].strip()
        name = name.title()
        if ', The' in name:
            name = 'The {0}'.format(name.replace(', The', ''))

        self.name = name
        
        # Type and titles
        candidates = {FILM: {}, TV: {}}
        film = TIMINGS[FILM]
        tv = TIMINGS[TV]

        for key, title in self.titles.items():
            self.log.debug('Title {0} ({1})'.format(key, title['duration']))
            if film['min'] <= title['duration'] <= film['max']:
                candidates[FILM][key] = title

            if tv['min'] <= title['duration'] <= tv['max']:
                candidates[TV][key] = title

        if len(candidates[FILM]) >= len(candidates[TV]):
            self.type = FILM
            self.titles = candidates[FILM]

        elif candidates[TV]:
            self.type = TV
            self.titles = candidates[TV]


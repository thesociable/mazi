# :coding: utf-8
# :copyright: Copyright (C) 2011 Martin Pengelly-Phillips
# :license: Proprietary. See LICENSE.txt for details.

import subprocess
import operator
import logging

from pyparsing import (alphas, Combine, Word, Group, Literal, restOfLine,
                       ZeroOrMore, SkipTo)


class Handbrake(object):
    '''Handbrake'''

    def __init__(self, handbrake_path='HandBrakeCLI'):
        '''Initialise.
        
        *handbrake_path* should be the location of the Handbrake command line
        exectuable.

        '''
        self.log = logging.getLogger('mazi.handbrake.Handbrake')
        self.handbrake_path = handbrake_path
        self._parser = None

    def call(self, *args, **kw):
        '''Call Handbrake CLI'''
        command = [self.handbrake_path]
        command.extend(args)
        for key, value in kw.items():
            key = key.replace('_', '-')
            key = '{0}{1}'.format('-'*(min(2, len(key))),
                                  key)
            
            if isinstance(value, bool):
                command.append(key)
            else:
                command.extend([key, value])

        command = map(str, command)
        self.log.debug(command)
        process = subprocess.Popen(command, stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
        return process
        
    def inspect(self, path):
        '''Inspect media at *path* and return information'''
        data = {'path':path, 'titles':{}}
        process = self.call(title=0, input=path)
        output = process.communicate()[0]
        
        matches = self.parser.scanString(output)
        for match, start, end in matches:
            data['titles'].setdefault(match.title, {})
            title = data['titles'][match.title]
            title['number'] = int(match.title)

            # Convert duration to seconds
            duration = sum(map(operator.mul, 
                               map(int, match.duration.split(':')),
                               [3600, 60, 1]))
            title['duration'] = duration

            # Convert subtitle data
            title['subtitles'] = dict(list(match.subtitles))

        return data

    @property
    def parser(self):
        '''Construct parser for handbrake output'''
        if self._parser is None:
            # From http://code.google.com/p/brakejob/source/browse/brakejob.py
            title = Literal('+ title').suppress()
            integer = Word('0123456789')
            time = Combine(integer + ':' + integer + ':' + integer)
            duration = Literal('+ duration:').suppress()
            subtitle = Literal('+ subtitle tracks:')
            iso = Literal('(iso639-2:').suppress() + Word(alphas)
            subtitle_track = Literal('+').suppress() + Group(integer + SkipTo(iso).suppress() + iso) + restOfLine.suppress()

            title_num = integer.setResultsName('title')
            duration_num = time.setResultsName('duration')
            subtitles = Group(ZeroOrMore(subtitle_track)).setResultsName('subtitles')

            pattern = title + title_num + \
                SkipTo(duration).suppress() + \
                duration + duration_num + \
                SkipTo(subtitle).suppress() + subtitle.suppress() + subtitles
            
            self._parser = pattern
    
        return self._parser


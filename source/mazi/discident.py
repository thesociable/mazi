# :coding: utf-8
# :copyright: Copyright (C) 2011 Martin Pengelly-Phillips
# :license: Proprietary. See LICENSE.txt for details.

import os
import glob
import hashlib
import re

FINGERPRINT_REG = re.compile('(.{8})(.{4})(.{4})(.{4})(.*)')


def is_dvd_file(name):
    return os.path.splitext(name)[-1] in ('.BUP', '.IFO', '.VOB')


def fingerprint(path):
    '''Compute DVD fingerprint for DVD at *path* as per http://discident.com/
    
    *path* should be the root of the DVD folder structure with a VIDEO_TS 
    directory as an immediate child.
    
    '''
    video_ts = os.path.join(path, 'VIDEO_TS')
    if not os.path.isdir(video_ts):
        raise ValueError('Path not valid, no VIDEO_TS subfolder found: '
                         '{0}'.format(path))

    # Compose data
    paths = sorted(filter(is_dvd_file, glob.glob('{0}/*'.format(video_ts))))
    pairs = []
    for entry in paths:
        pairs.append(':'.join((entry.replace(path, ''), 
                               str(os.path.getsize(entry)))))

    data = ':{0}'.format(':'.join(pairs))
    
    # Compute fingerprint
    fingerprint = hashlib.md5(data).hexdigest().upper()
    
    # Human readable
    fingerprint = FINGERPRINT_REG.sub(r'\1-\2-\3-\4-\5', fingerprint)

    return fingerprint


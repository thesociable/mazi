# :coding: utf-8
# :copyright: Copyright (C) 2011 Martin Pengelly-Phillips
# :license: Proprietary. See LICENSE.txt for details.

import sys
import os
import re
from argparse import ArgumentParser
import logging

from mazi.handbrake import Handbrake
from mazi.media import Media, FILM, TV, UNKNOWN
    
PROGRESS_REG = re.compile('Encoding:.*, (\d+\.\d+) %.*(ETA.+)\)')


def main(arguments=None):
    '''Mazi'''
    logging.basicConfig(level=logging.DEBUG, format='%(message)s')
    log = logging.getLogger('mazi.entry_point.main')

    parser = ArgumentParser()
    parser.add_argument('source', help='Path holding dvd media')
    parser.add_argument('destination', help='Path to store output')
    parser.add_argument('record', help='Path for record keeping')
    parser.add_argument('--convert', help='Perform conversion.', default=False,
                        action='store_true')
    parser.add_argument('--language', help='Native language.', default='eng')
    parser.add_argument('--handbrake', default='HandBrakeCLI',
                        help='Path to Handbrake executable')
    
    args = parser.parse_args(arguments)

    handbrake = Handbrake(handbrake_path=args.handbrake)
    paths = search(args.source)
    
    if not os.path.exists(args.destination):
        os.makedirs(args.destination)

    total = 0
    failed = 0
    
    for path in paths:
        data = handbrake.inspect(path)
        media = Media(**data)
        log.info(media)
        if media.type == UNKNOWN:
            continue
        
        if not args.convert:
            continue

        title_count = len(media.titles)
        for index, (number, title) in enumerate(sorted(media.titles.items())):
            total += 1

            if title_count > 1:
                output = '{0}.{1:02d}'.format(media.safe_name, int(index)+1)
            else:
                output = media.safe_name

            receipt = os.path.join(args.record, output+'.txt')
            if os.path.exists(receipt):
                continue

            output = os.path.join(args.destination, output+'.mkv')
            log.info('Generating {0}'.format(output))
            process = handbrake.call(input=path,
                                     output=output,
                                     title=number,
                                     preset='High Profile',
                                     native_language=args.language, 
                                     subtitle='scan',
                                     subtitle_forced='scan',
                                     markers=True)
            pid = process.pid
            ret = process.poll()
            
            encode_log = ''
            while ret is None:
                read = process.stdout.read(100)
                match = PROGRESS_REG.search(read)
                if match:
                    progress = ' Progress: ' + match.group(1) +' '+match.group(2) + '   \r'
                    sys.stdout.write(progress)
                    sys.stdout.flush()
                encode_log += read
                ret = process.poll()
                    

            encode_log += process.stdout.read()
            print ''
            if ret == 0:
                print 'Encoded successfully {0}'.format(output)
                with open(receipt, 'w') as fd:
                    fd.write(encode_log)
            else:
                failed += 1
                print 'Encoding failed: {0}'.format(encode_log)
                if os.path.exists(output):
                    os.remove(output)
    
    log.info("Total: %d, Failed: %d" % (total, failed))


def search(path):
    '''Recursively search *path* for source media'''
    paths = []
    for directory, directories, filenames in os.walk(path):
        directories[:] = filter(lambda d: not d.startswith('.'),
                                directories)
        
        for filename in filenames:
            ext = os.path.splitext(filename)[-1]
            if ext in ('.iso', ):
                paths.append(os.path.join(directory, filename))
        
        if os.path.basename(directory).lower() == 'video_ts':
            paths.append(os.path.dirname(directory))
            del directories[:]
    
    return paths
